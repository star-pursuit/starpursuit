# User stories
These are the userstories for Star pursuite. For the game to be complete userstories all userstories for version 1 should be done.

## Format:

### Title of story (us-x)
**The want**
What does this do

A userstory should follow INVEST:
* I: Independent. User stories don’t have to be implemented in a particular order, because
they are independent of each other. Even though dependencies cannot be avoided sometimes, they should be kept at a minimum, so that stories can be implemented in the order
of their business value.
* N: Negotiable. User stories should leave space for negotiations between business and
development. Those negotiations can help to keep the cost low by agreeing on simple
features and easy implementations.
* V: Valuable. User stories must create clear and quantifiable value to the business. Soft
quantifications like high/medium/low are fine, as long as stories can be compared in
terms of their business value. Such stories usually cut through all layers: from frontend
over backend to the database and middleware. Architecture, refactoring, and cleanup
tasks are not user stories!
* E: Estimable. User stories must be concrete enough in order to be estimated by the developers. However, stories must still be negotiable, so aim for the sweet spot between
specificy and vagueness by being precise about the business value while leaving out implementation details.
* S: Small. User stories should be small enough so that they can be implemented by one
or two developers within a single iteration. A good rule of thumb is to pick roughly the
same number of stories for an iteration as there are developers on the team.
* T: Testable. User stories should be accompanied by tests specified by the business. A story
is complete when all of its tests pass. Tests are usually written by QA and automated by
the developers. Specifying the tests can happen later than the actual story is written.

## Backlog version 1
Version 1 is going to focus on planetside.

### Get an overview of my resources (us-1)
**As a user I want to see how much resources I have**
There should be a menu or something to see how many resources one has

#### Importent to see

#### Importent to interact with

### What does the resources do (us-2)
**As a user I need to know what the different resoursces can be used for**
When hovering a resource get description or have a menu that can explain it.

### Create buildings (us-3)
**As a user I want to gather more resources and have more to do on a planet**

### What does the buildings do/produce (us-3)
**As a user I want to know what the different buildings can do for me**
When hovering a building give the user an description or give the productionchain

### Demolish/change buildings
**As a user if I build a building I want to be able to remove it or change**
If a user makes wrong building or finds that they rather want to get other resources they should be able to destroy the building.

### Constraints on amount of buildings

To create a need to expand there should be a cap on how many building a planet can have on it.

### Buildings need workforce
**Why have a population if they can not be used**
The building in the game should be maned by population

### Population needs and growth
**To have more buildings I need more populations**
The game should be able to create more populations and there should be some conflict to stop the user from making infinit amount of populations

### Conflict with Planets population
**I want to be encureage to treat my populations good/bad so i should care**
When treated badly pops do not work as productivly and might stop being loyal.

### Crime and revolts
**As a user I want my actions to have consequences. When i treat my population badly I need a reaction.**
Populations that are criminal or rebelious should be able to steal resources and hide them in staches so rebelplayer can get them for free.
If all pops on planet are either criminals or rebels they can try to take a hostile takeover.
They will try to attackt the forces protecting the planet and if they win they take the planet, either it becomes a pirate world or rebel world.

A Rebelworld will be viseble for the rebel player.

### Combat crime and revolts
**As a user I want to have some agancy and after misshaps and revolts I need to fix my mistakes**
New buildings.
new screen on planet. a combate screen.
New actions
* Add Military posts, information departments to get to know witch populations are revolutionary/criminal, and to be able to make them loyal agine. 
* Find criminal/rebel staches of goods by making military post/information departments do frisk searches (Angers populus). If one is found can fight the singular rebel/criminal pop and getting the resources back.


### Sound Effects
**As a user I want to get audio clues to my actions. When a building is built i want sound, or when construction starts, esspecialy when a riot breakes out**


## Backlog version 2
Version 2 will be characterised by the space and space travel.

### Generate a galaxy
**As a user I want to have options on where to go and explore**
Should generate a space map with all planets, each planet should get NaturalResources, size and a name.
How to traverse them could either be spacelanes (e.i edges between nodes) or just if the ship has enoght foul for the journy.
What is most fun what is best for both players

### Change name of planets
**As a user I want to change the name of a system to be more meaningful for me**

### Explore/surway another the planet

### Collonise other planets

### Ships should take the shortest path

### Transfere resources across planets

### Traderoutes

## Backlog version 3
Version 3 will be characterised by ships needing requirements like fuel and food.

### Ships should have a cost

The ships should require food and foul. When a ship does not have foul it should not be able to travel between stars, and no food will kill the pop and a new crew will be required

### Differet ships diffrent attributes

Bigger ships for transporting more goods


## Backlog version 4
Version 4 will be characterised by Navel combate

### Sound effects for combate


## Backlog version 5
Version 5 will be characterised by Pirates

## Backlog version 6
Version 6 will be characterised by other empires
and diplomecy

### The empire should spawn in the start of the game

### Diplomacy screen:


### Dipomatic actions:
* Give gift
* Declare war
* Cease-fire
* Compliment
* Insult
* Ally
* Get information on other player

Influence points


